# Usa la imagen de Node.js como base
#FROM node:18-alpine
FROM node:16.20.2

# Instala Git
RUN apt-get update && apt-get install -y git

# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia los archivos del sitio Gatsby al contenedor
COPY . /app

# Instala Gatsby CLI globalmente
RUN npm install -g gatsby-cli
RUN npm install -g gatsby@3.14.1

# Instala las dependencias del sitio
RUN npm install

# Ejecuta los comandos para crear el sitio estático
CMD ["gatsby", "develop"]